import './style.css'
import { fabric } from 'fabric'

import { $ } from './helpers/selectors'

import colorSelector from './components/colorSelector'
import lineSelector from './components/lineSelector'
import undoButton from './components/undoButton'
import redoButton from './components/redoButton'

// Update functions
const updateSelectedColor = color => {
  state.canvas.freeDrawingBrush.color = color
}
const updateLineThickness = thickness => {
  state.canvas.freeDrawingBrush.width = thickness
}

// Init state, including Fabric.js canvas
const state = {
  selectedColor: null,
  selectedWeight: null,
  canvas: new fabric.Canvas('canvas', {
    isDrawingMode: true
  }),
  history: [], // should limit this
  redoing: false
}

// Declare and init our components
const colorList = colorSelector(updateSelectedColor)
const lineList = lineSelector(updateLineThickness)
const undo = undoButton('↩️', state)
const redo = redoButton('↪️', state)
// Push empty canvas state
state.history.push(state.canvas.toDatalessJSON())
// Listen on canvas state changes
state.canvas.on('object:added', () => {
  if (!state.redoing) state.history = []
  state.redoing = false
})

// Append components to the container
$('color-tool').appendChild(colorList)
$('line-tool').appendChild(lineList)
$('undo-tool').appendChild(undo)
$('redo-tool').appendChild(redo)

// Pre-select black
$('Black').click()
// Pre-select thin
$('regular').click()
