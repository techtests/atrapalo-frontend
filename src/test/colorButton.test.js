import 'jsdom-global/register'
import { expect } from 'chai'

import colorButton from '../components/colorButton'

describe('colorButton component', () => {
  let button = null

  beforeEach(done => {
    button = colorButton(
      'Red',
      '#F00',
      () => null
    )
    done()
  })

  it('should return a <li> element', done => {
    expect(button).to.be.an('HTMLLIElement')
    done()
  })
  it('should have color-button class', done => {
    expect(button.getAttribute('class')).to.equal('color-button')
    done()
  })
  it('should have id equal to the defined name \'Red\'', done => {
    expect(button.getAttribute('id')).to.equal('Red')
    done()
  })
  it('should have a background color equal to the rgb value (rgb(255, 0, 0) of defined hex \'#F00\'', done => {
    expect(button.style.backgroundColor).to.equal('rgb(255, 0, 0)')
    done()
  })
  it('should add the \'selected\' class to the element when onclick()', done => {
    button.onclick()
    expect(button.getAttribute('class')).to.equal('color-button selected')
    done()
  })
})
