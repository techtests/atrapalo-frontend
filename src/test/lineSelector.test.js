import 'jsdom-global/register'
import { expect } from 'chai'

import lines from '../constants/lines'
import lineSelector from '../components/lineSelector'

describe('lineSelector component', () => {
  let selector = null

  beforeEach(done => {
    selector = lineSelector(() => null)
    done()
  })

  it('should return a <ul> element', done => {
    expect(selector).to.be.an('HTMLUListElement')
    done()
  })
  it('should have line-selector-list class', done => {
    expect(selector.getAttribute('class')).to.equal('line-selector-list')
    done()
  })
  it('should have a selectedThickness property', done => {
    expect(selector).to.have.property('selectedThickness')
    done()
  })
  it('should have as many children as lines in the constants file', done => {
    expect(selector.childNodes.length).to.equal(Object.keys(lines).length)
    done()
  })
  it('should return the hex code for the color on getSelectedThickness()', done => {
    expect(selector.getSelectedThickness()).to.equal(lines.thin)
    done()
  })
  it('should correctly change the current thickness on onThicknessChange()', done => {
    expect(selector.getSelectedThickness()).to.equal(lines.thin)
    selector.onThicknessChange(lines.regular)
    expect(selector.getSelectedThickness()).to.equal(lines.regular)
    done()
  })
})
