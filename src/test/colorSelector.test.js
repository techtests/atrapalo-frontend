import 'jsdom-global/register'
import { expect } from 'chai'

import colors from '../constants/colors'
import colorSelector from '../components/colorSelector'

describe('colorSelector component', () => {
  let selector = null

  beforeEach(done => {
    selector = colorSelector(() => null)
    done()
  })

  it('should return a <ul> element', done => {
    expect(selector).to.be.an('HTMLUListElement')
    done()
  })
  it('should have color-selector-list class', done => {
    expect(selector.getAttribute('class')).to.equal('color-selector-list')
    done()
  })
  it('should have a selectedColor property', done => {
    expect(selector).to.have.property('selectedColor')
    done()
  })
  it('should have as many children as colors in the constants file', done => {
    expect(selector.childNodes.length).to.equal(Object.keys(colors).length)
    done()
  })
  it('should return the hex code for the color on getSelectedColor()', done => {
    expect(selector.getSelectedColor()).to.equal(colors.Black)
    done()
  })
  it('should correctly change the current color on onColorChange()', done => {
    expect(selector.getSelectedColor()).to.equal(colors.Black)
    selector.onColorChange(colors.Red)
    expect(selector.getSelectedColor()).to.equal(colors.Red)
    done()
  })
})
