import 'jsdom-global/register'
import { expect } from 'chai'

import lineButton from '../components/lineButton'

describe('lineButton component', () => {
  let button = null

  beforeEach(done => {
    button = lineButton(
      'regular',
      3,
      () => null
    )
    done()
  })

  it('should return a <li> element', done => {
    expect(button).to.be.an('HTMLLIElement')
    done()
  })
  it('should have line-button class', done => {
    expect(button.getAttribute('class')).to.equal('line-button')
    done()
  })
  it('should have id equal to the defined name \'regular\'', done => {
    expect(button.getAttribute('id')).to.equal('regular')
    done()
  })
  it('should have a height equal to 3px', done => {
    expect(button.style.height).to.equal('3px')
    done()
  })
  it('should add the \'selected\' class to the element when onclick()', done => {
    button.onclick()
    expect(button.getAttribute('class')).to.equal('line-button selected')
    done()
  })
})
