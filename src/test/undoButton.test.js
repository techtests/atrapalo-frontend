import 'jsdom-global/register'
import { expect } from 'chai'

import undoButton from '../components/undoButton'

describe('undoButton component', () => {
  let state = {}
  let button = null
  beforeEach(done => {
    state = {
      canvas: {
        _objects: ['test'],
        renderAll: () => null
      },
      history: [],
      redoing: false
    }
    button = undoButton('Undo', state)
    done()
  })

  it('should return a <div> element', done => {
    expect(button).to.be.an('HTMLDivElement')
    done()
  })
  it('should have undo-button id', done => {
    expect(button.getAttribute('id')).to.equal('undo-button')
    done()
  })
  it('should have undo-button class', done => {
    expect(button.getAttribute('class')).to.equal('undo-button')
    done()
  })
  it('should display \'Undo\' in its innerHTML', done => {
    expect(button.innerHTML).to.equal('Undo')
    done()
  })
  it('should pop an item from state.canvas._objects and push it in state.history on onclick', done => {
    expect(state.canvas._objects.length).to.equal(1)
    expect(state.history.length).to.equal(0)
    button.onclick()
    expect(state.canvas._objects.length).to.equal(0)
    expect(state.history.length).to.equal(1)
    done()
  })
})
