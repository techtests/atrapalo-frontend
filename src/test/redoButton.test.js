import 'jsdom-global/register'
import { expect } from 'chai'

import redoButton from '../components/redoButton'

describe('redoButton component', () => {
  let state = {}
  let button = null
  beforeEach(done => {
    state = {
      canvas: {
        _objects: [],
        add: function (item) { // needs 'this' context to push
          this._objects.push(item)
        }
      },
      history: ['test'],
      redoing: false
    }
    button = redoButton('Redo', state)
    done()
  })

  it('should return a <div> element', done => {
    expect(button).to.be.an('HTMLDivElement')
    done()
  })
  it('should have redo-button id', done => {
    expect(button.getAttribute('id')).to.equal('redo-button')
    done()
  })
  it('should have redo-button class', done => {
    expect(button.getAttribute('class')).to.equal('redo-button')
    done()
  })
  it('should display \'Redo\' in its innerHTML', done => {
    expect(button.innerHTML).to.equal('Redo')
    done()
  })
  it('should pop an item from state.history and push it in state.canvas._objects, setting redoing to true on onclick', done => {
    expect(state.canvas._objects.length).to.equal(0)
    expect(state.history.length).to.equal(1)
    expect(state.redoing).to.equal(false)
    button.onclick()
    expect(state.canvas._objects.length).to.equal(1)
    expect(state.history.length).to.equal(0)
    expect(state.redoing).to.equal(true)
    done()
  })
})
