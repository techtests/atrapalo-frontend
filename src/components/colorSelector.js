import colors from '../constants/colors'
import colorButton from './colorButton'

export default (updateSelectedColor, el = document.createElement('ul')) => {
  const props = { selectedColor: colors.Black }

  Object.assign(el, props)
  el.setAttribute('class', 'color-selector-list')

  el.render = () => {
    Object.keys(colors).forEach(name => {
      el.appendChild(colorButton(
        name,
        colors[name],
        el.onColorChange))
    })
  }

  el.onColorChange = color => {
    el.selectedColor = color
    updateSelectedColor(color)
  }
  el.getSelectedColor = () => el.selectedColor

  el.render()

  return el
}
