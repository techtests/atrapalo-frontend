import { $$ } from '../helpers/selectors'

export default (name, thickness, onThicknessChange, el = document.createElement('li')) => {
  const props = { name, thickness }

  Object.assign(el, props)

  el.setAttribute('id', el.name)
  el.setAttribute('class', 'line-button')

  el.style.height = `${el.thickness}px`

  el.onclick = () => {
    if ($$('line-button selected')) $$('line-button selected').setAttribute('class', 'line-button')
    el.setAttribute('class', 'line-button selected')
    onThicknessChange(el.thickness)
  }

  return el
}
