import lines from '../constants/lines'
import lineButton from './lineButton'

export default (updateSelectedThickness, el = document.createElement('ul')) => {
  const props = { selectedThickness: lines.thin }

  Object.assign(el, props)
  el.setAttribute('class', 'line-selector-list')

  el.render = () => {
    Object.keys(lines).forEach(name => {
      el.appendChild(lineButton(
        name,
        lines[name],
        el.onThicknessChange))
    })
  }

  el.onThicknessChange = thickness => {
    el.selectedThickness = thickness
    updateSelectedThickness(thickness)
  }
  el.getSelectedThickness = () => el.selectedThickness

  el.render()

  return el
}
