export default (text = 'Undo', state, el = document.createElement('div')) => {
  const props = { text }

  Object.assign(el, props)

  el.setAttribute('id', 'undo-button')
  el.setAttribute('class', 'undo-button')
  el.innerHTML = el.text

  el.onclick = () => {
    if (!state.canvas._objects.length) return
    state.history.push(state.canvas._objects.pop())
    state.canvas.renderAll()
  }

  return el
}
