import { $$ } from '../helpers/selectors'

export default (name, hex, onColorChange, el = document.createElement('li')) => {
  const props = { name, hex }

  Object.assign(el, props)

  el.setAttribute('id', el.name)
  el.setAttribute('class', 'color-button')

  el.style.backgroundColor = el.hex

  el.onclick = () => {
    if ($$('color-button selected')) $$('color-button selected').setAttribute('class', 'color-button')
    el.setAttribute('class', 'color-button selected')
    onColorChange(el.hex)
  }

  return el
}
