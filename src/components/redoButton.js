export default (text = 'Redo', state, el = document.createElement('div')) => {
  const props = { text }

  Object.assign(el, props)

  el.setAttribute('id', 'redo-button')
  el.setAttribute('class', 'redo-button')
  el.innerHTML = el.text

  el.onclick = () => {
    if (!state.history.length) return
    state.redoing = true
    state.canvas.add(state.history.pop())
  }

  return el
}
