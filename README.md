# Atrápalo
## Frontend Technical Test

Tienes que hacer una aplicacion parecida al famoso paint ~~de Microsoft~~. Esta aplicación
deberá tener solo las siguientes funcionalidades:
* Deshacer/rehacer
* Selección de color
* Grueso de línea

## To do
- [x] Create repo
- [x] Init Readme
- [x] Decide: vanilla ~~frameworks~~
- [x] Handmade 4 Webpack Configuration
- [x] Build testing pipeline
- [x] Write tests
- [x] Build the actual app

## Installation and Usage

After cloning the repository, run `npm install` to install dependencies; `npm start` to open a webpack-dev-server on default port; `npm run build` to build a dist version

## Testing

After cloning the repository, run `npm run test` to test all components using mocha, chai and jsdom

## Considerations

1. No CSS preprocessors will be used, as our app will have minimal styling and the setup & build times are just not worth it right now. They can be added later as more features are required and re-use of styles becomes necessary.
